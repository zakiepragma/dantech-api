<?php

namespace App\Http\Requests\API;

use App\Models\Product;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => ['required'],
            'quantity' => ['required'],
            'price' => ['required'],
        ];
    }

    public function store()
    {
        $product = Product::create($this->all());

        return $product;
    }

    public function update($id)
    {
        $product = Product::findOrFail($id);

        $product->fill($this->all());

        $product->save();

        return $product;
    }
}